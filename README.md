#Wei.Lib2A

Android快速开发框架，Android常用工具集。
数据加载框架和下载框架并未集成，待续。。。（大部分东西在去年都写好了，一直懒得整理）

##这个库都有什么？能帮我们做什么？

####我写代码遵循两个基本原则：

>1、减少代码量。希望达到的效果是，让使用者尽可能减少代码，能一句话搞定的，绝不两句，能静态方法搞定的，绝不new对象；<br>
>2、增强适应性和稳定性。由于Android平台厂商定制的碎片化和众多版本兼容性问题，本库的开发会着重考虑这些因素，而且
基本都是经上线的项目考验过的。

####那么先说说几个我个人认为最好用的组件:
#####1、存储卡工具类 [Storage.java](http://git.oschina.net/wei.chou/Wei.Lib2A/blob/master/Wei.Lib2A/src/com/wei/c/phone/Storage.java)
仅通过几个常量即可便捷的取得内置或外置 [Storage.SdCard](http://git.oschina.net/wei.chou/Wei.Lib2A/blob/master/Wei.Lib2A/src/com/wei/c/phone/Storage.java)
对象。例如：
```java

//输出外置存储卡的路径到logcat
if (Storage.CARD_EXT != null) L.i(this, Storage.CARD_EXT.path);

//是否可以在默认的卡上创建任意目录
if (Storage.CARD_DEF.isCustomDirCreatable(context)) {
    //...
    try {
        File dir = FileUtils.makeDir(dirPath, true);
        L.i(this, dir.getPath());
        //...
    } catch (FileCreateFailureException e) {
        L.e(this, e);
    }
}

```






######待续。。。


###欢迎加入我的 [QQ群:215621863](http://shang.qq.com/wpa/qunwpa?idkey=c39a32d6e9036209430732ff38f23729675bf1fac1d3e9faac09ed2165ae6e17 "Android编程&移动互联") 相互学习探讨！
